import os
from flask import Flask, request, jsonify
from flask_pymongo import PyMongo

application = Flask(__name__)

application.config["MONGO_URI"] = 'mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ['MONGODB_PASSWORD'] + '@' + os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE']

mongo = PyMongo(application)
db = mongo.db

############################################################
#                   APIs COLLECTIONS RESIDANTS             # 
############################################################

@application.route('/residant', methods=['GET'])
def getAllRes():
    residants = db.residants.find()
    data = [{'id': str(res['_id']), 'nom': res['nom'], 'appart': res['appart'], 'tel': res['tel']} for res in residants]
    return jsonify(
        status=True,
        data=data
    ), 200

@application.route('/residant/<string:id>', methods=['GET'])
def getResById(id):
    residants = db.residants.find()
    data = [{'id': str(res['_id']), 'nom': res['nom'], 'appart': res['appart'], 'tel': res['tel']} for res in residants if str(res['_id']) == id]
    return jsonify(
        status=True,
        data= data
    ), 200

@application.route('/residant', methods=['POST'])
def addRes():
    data = request.get_json(force=True)
    res = {
        'nom': data['nom'],
        'tel': data['appart'],
        'appart': data['appart']
    }
    db.residants.insert_one(res)
    return jsonify(
        status=True,
        message='Residant ajouté!'
    ), 201

############################################################
#                    APIs COLLECTIONS SALLES               # 
############################################################

@application.route('/salle', methods=['GET'])
def getAllSc():
    salles = db.salles.find()
    data = [{'id': str(sl['_id']), 'nom': sl['nom'], 'nbrecle': sl['nbrecle']} for sl in salles]
    return jsonify(
        status=True,
        data=data
    ),200

@application.route('/salle/<string:id>', methods=['GET'])
def getSalleById(id):
    salles = db.salles.find()
    data = [{'id': str(sl['_id']), 'nom': sl['nom'], 'nbrecle': sl['nbrecle']} for sl in salles if str(sl['_id']) == id]
    return jsonify(
        status=True,
        data= data
    ), 200

@application.route('/salle', methods=['POST'])
def addSalle():
    data = request.get_json(force=True)
    res = {
        'nom': data['nom'],
        'nbrecle': data['nbrecle']
    }
    db.salles.insert_one(res)
    return jsonify(
        status=True,
        message='Salle commune ajoutée!'
    ), 201
    

############################################################
#                   APIs COLLECTIONS RESERVATIONS          # 
############################################################


@application.route('/reservation', methods=['GET'])
def getAllRsv():
    reservations = db.reservations.find()
    data = [{'id': str(res['_id']), 'salle': res['salle'], 'residant': res['residant'], 'date': res['date']} for res in reservations]
    return jsonify(
        status=True,
        data=data
    ),200

@application.route('/reservation/<date>/<salle>', methods=['GET'])
def getRsvByDateSalle(date, salle):
    reservations = db.reservations.find()
    data = [{'id': str(res['_id']), 'salle': res['salle'], 'residant': res['residant'], 'date': res['date']} for res in reservations if  res['date'] == date and res['salle'] == salle]
    return jsonify(
        status=True,
        data=data
    ),200


@application.route('/reservation', methods=['POST'])
def addRsv():
    data = request.get_json(force=True)
    reservations = db.reservations.find()
    salles = db.salles.find()

    nbreRes = 0
    nbreCle = 0

    for sl in salles:
        if str(sl['_id']) == data['salle']:
            nbreCle = int(sl['nbrecle'])
            break

    for res in reservations:
        if  res['date'] == data['date'] and res['salle'] == data['salle'] :
            nbreRes += 1
            if nbreCle == nbreRes:
                return jsonify(
                    status=True,
                    message='Salle indisponible ce jour!'
                ), 200
    
    res = {
        'salle': data['salle'],
        'date': data['date'],
        'residant': data['residant']
    }

    db.reservations.insert_one(res)
    return jsonify(
        status=True,
        message='Reservation ajouté ajoutée!'
    ), 201

if __name__ == "__main__":
    ENVIRONMENT_DEBUG = os.environ.get("APP_DEBUG", True)
    ENVIRONMENT_PORT = os.environ.get("APP_PORT", 5000)
    application.run(host='0.0.0.0', port=ENVIRONMENT_PORT, debug=ENVIRONMENT_DEBUG)