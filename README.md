# reservationsalleapi

Test de docker et flask.
APIs permettant de faire des ajout et des listages de :
- Résidants
- Salles communes
- Reservations.

## Comment tester

Création d'images, de conteneurs (ainsi que démarrage)

    $	cd reservationapi/
    $	docker-compose up -d
  
  Configuration de mongodb :
  - Connexion

    $	docker exec -ti rsdbserver bash
    
    mongo -u superuser

Le mot de passe est le l'utilisateur superuser:  ***superuserpsw***

- Création de la base de donnée **rsdb** et de l'utilisateur **rsdbu**


    use rsdb
    
    db.createUser({user: 'rsdbu', pwd: 'rsdbpsw', roles: [{role: 'readWrite', db: 'rsdb'}]})

vous pouvez vous connecter pour être sur en faisant:

    mongo -u rsdbu -p rsdbpsw --authenticationDatabase rsdb

**JEU DE TEST**

Maintenant que la base de donnée est créée, vous pouvez utiliser un outil de test d'api (Postman par exemple) pour tester les API
	
    1. Enregistrement de residant :  
	Host: localhost:1207/residant
	Methode: POST  
	Exple de donnee: 
	{
		"nom": "BESSÉ YAO",
		"tel": "0707070707",
		"appart": "310"
	}
	
	2. Liste de residant:
	Host: localhost:1207/residant
	Methode: GET
	
	3. Resident par Identifiant:
	Host: localhost:1207/residant/<id>
	Methode: GET
	
	4. Enregistrement de salle :  
	Host: localhost:1207/salle
	Methode: POST  
	Exple de donnee: 
	{
		"nom": "Salle de sport",
		"nbrecle": "3"
	}
	
	5. Liste de salle:
	Host: localhost:1207/salle
	Methode: GET
	
	6. Salle par Identifiant:
	Host: localhost:1207/salle/<id>
	Methode: GET
	
	7. Enregistrement de reservation :  
	Host: localhost:1207/reservation
	Methode: POST  
	Exple de donnee: 
	{
		"salle": "601817d0e756274a840e2f9e",
		"date": "03-02-2020",
		"residant": "60182c580e5745007306f234"
	}
	
	5. Liste de reservation:
	Host: localhost:1207/reservation
	Methode: GET
	
	6. Reservation par date et salle:
	Host: localhost:1207/salle/<date>/<isdalle>
	Methode: GET
